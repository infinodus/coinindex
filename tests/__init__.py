from pathlib import Path
from wcdx.config import settings

DATA = Path(__file__).parent / 'data'

settings.set('clickhouse', 'database', 'test')
