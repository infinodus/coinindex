import logging
import aiohttp
import async_timeout
from wcdx.config import settings

logger = logging.getLogger(__name__)

API_URL = 'https://www.worldcoinindex.com/apiservice/json'

# Alternative source: https://coinmarketcap.com/api/


async def fetch(session, url: str, params: dict=None, timeout: int=10):
    with async_timeout.timeout(timeout):
        async with session.get(url, params=params) as response:
            return await response.json()


async def get_data():
    params = {
        'key': settings.get('coinindex', 'key')
    }
    async with aiohttp.ClientSession() as session:
        data = await fetch(session, API_URL, params=params)
    return data
