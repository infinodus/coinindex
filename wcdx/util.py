__all__ = ['executor', 'set_cancel_handler']

import signal
import asyncio
import logging
import functools
import concurrent.futures
from wcdx.config import settings

logger = logging.getLogger(__name__)


def set_executor(cpu_count: int=None) -> functools.partial:
    """wrap an executor
    """
    def execute(func, *args, loop=None, executor=None, **kwargs):
        loop = loop or asyncio.get_event_loop()
        wrap = functools.partial(func, *args, **kwargs)
        return loop.run_in_executor(executor, wrap)

    workers = settings.getint('executor', 'workers', fallback=3)
    executor = \
        concurrent.futures.ThreadPoolExecutor(max_workers=workers)
    logger.info('creating {} with {} workers'.format(
        executor.__class__.__name__, workers))

    return functools.partial(execute, executor=executor)


executor = set_executor()


def as_future(func):
    """Wrap an a synchronous method in a future

    Decorator for running a synchronous method in an executor context.
    """
    @functools.wraps(func)
    def wrapped(*args, **kwargs):
        return executor(func, *args, **kwargs)
    return wrapped


def set_cancel_handler(loop):
    """Set a cancel handler
    """
    async def shutdown(sig, loop):
        """Shutdown all tasks
        """
        logger.warning(
            '{} received, shutting down'.format(sig.name))

        tasks = [
            task for task in asyncio.Task.all_tasks()
            if task is not asyncio.tasks.Task.current_task()
        ]

        list(map(lambda task: task.cancel(), tasks))

        results = await asyncio.gather(
            *tasks, return_exceptions=True)

        logger.warning('{} tasks cancelled'.format(results))
        loop.stop()

    loop.add_signal_handler(
        signal.SIGTERM,
        functools.partial(
            asyncio.ensure_future, shutdown(signal.SIGTERM, loop)))
