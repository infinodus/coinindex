__all__ = ['settings']

import imp
from pathlib import Path
from configparser import ConfigParser, ExtendedInterpolation


def as_list(value: str) -> list:
    """Return a list of strings

    Parse a value by separating the input based
    on newline and/or comma separated.
    """
    # split on newlines
    value = filter(None, [x.strip() for x in value.splitlines()])
    # split on comma
    value = filter(None, [x.strip() for v in value for x in v.split(',')])
    return list(value)


class Settings(ConfigParser):
    def setup(self, value):
        if isinstance(value, str):
            path = Path(value)
            if path.is_file():
                with path.open() as handle:
                    self.read_file(handle)
            else:
                self.read_string(value)
        if isinstance(value, dict):
            self.read_dict(value)


settings = Settings(
    converters={'list': as_list}, interpolation=ExtendedInterpolation())

# setup defaults
with Path(imp.find_module('wcdx')[1], 'data/default.ini').open() as handle:
    settings.read_file(handle)
