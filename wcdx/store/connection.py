from .models import metadata
from wcdx.util import as_future
from wcdx.config import settings
from sqlalchemy.engine import url
from sqlalchemy import create_engine


@as_future
def bind_metadata(engine, loop=None):
    if not metadata.is_bound():
        metadata.bind = engine
        metadata.create_all()


async def init_db(loop=None):
    params = dict(settings.items('clickhouse'))
    engine = create_engine(url.URL('clickhouse', **params))

    await bind_metadata(engine, loop)
    return engine
