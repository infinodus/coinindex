from sqlalchemy import types
from datetime import datetime, timezone
from clickhouse_sqlalchemy.types import Float64
from sqlalchemy import Column, String, Date, MetaData
from clickhouse_sqlalchemy import engines, get_declarative_base

metadata = MetaData()
Base = get_declarative_base(metadata=metadata)


def from_timestamp(context):
    """Get date from timestamp

    Context sensitive conversion of timestamp to date.
    """
    value = context.get_current_parameters().get('timestamp')
    if isinstance(value, datetime):
        return value.date()
    return value.split()[0]  # 0000-00-00 00:00:00


class DateTime(types.TypeDecorator):
    """Clickhouse DateTime implementation
    """
    impl = types.DateTime

    def process_bind_param(self, value, dialect):
        return value.strftime('%Y-%m-%d %H:%M:%S')

    def process_result_value(self, value, dialect):
        value = datetime.strptime(value, '%Y-%m-%d %H:%M:%S')
        return value.replace(tzinfo=timezone.utc)


class Point(Base):
    date = Column(
        Date,
        primary_key=True,
        default=from_timestamp)
    currency = Column(String)
    quote = Column(String)
    value = Column(Float64)
    timestamp = Column(DateTime)

    __tablename__ = 'point'
    __table_args__ = (
        engines.MergeTree(
            'date', ('currency', 'quote', 'timestamp'), index_granularity=8192),  # noqa
    )


class Volume(Base):
    date = Column(
        Date,
        primary_key=True,
        default=from_timestamp)
    currency = Column(String)
    value = Column(Float64)
    timestamp = Column(DateTime)

    __tablename__ = 'volume'
    __table_args__ = (
        engines.MergeTree(
            'date', ('currency', 'timestamp'), index_granularity=8192)
    )
