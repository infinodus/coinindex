__all__ = ['MemoryTopicQueue']

import asyncio
import logging
from .base import TopicQueue
from typing import NamedTuple, Callable, Any

logger = logging.getLogger(__name__)


class Message(NamedTuple):
    body: Any = None
    requeue: Callable = None
    finish: Callable = lambda: None


class MemoryTopicQueue(asyncio.Queue, TopicQueue):
    """A memory topic queue
    """
    def __init__(self, topic: str, *, channel: str=None, **kwargs):
        super().__init__(**kwargs)

    async def connect(self) -> bool:
        return True

    def get_nowait(self) -> Message:
        value = super().get_nowait()
        return Message(value, lambda: self.put_nowait(value))

    def _init(self, maxsize: int):
        """
        """
        if self.channel:
            logger.warning(
                '{} does not support channels'.format(
                    self.__class__.__name__))
        self._queue = self._queues[self.topic]
