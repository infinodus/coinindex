import abc
import logging
from functools import lru_cache
from wcdx.config import settings
from typing import Dict, Optional, Any
from collections import defaultdict, deque

logger = logging.getLogger(__name__)


class NotConnectedError(Exception):
    """Not connected error
    """


class MessageContext:
    """Message handling context

    Either finalize a message or requeue on error.
    """
    def __init__(self, message):
        self.message = message

    async def __aenter__(self):
        return self.message

    async def __aexit__(self, exc_type, exc, tb):
        if exc:
            return self.message.requeue()
        self.message.finish()


class TopicQueue(metaclass=abc.ABCMeta):
    """
    """
    topic: str
    channel: Optional[str] = None
    _queues: Dict[str, deque] = defaultdict(deque)
    NotConnectedError: Exception = NotConnectedError

    @abc.abstractmethod
    async def get(self) -> Any:
        """Get an item
        """

    @abc.abstractmethod
    async def put(self, value: Any):
        """Put an item
        """

    @abc.abstractmethod
    async def connect(self) -> bool:
        """Create a connection
        """

    def finalize(self, message) -> MessageContext:
        """Return a message handling context
        """
        return MessageContext(message)

    def __new__(cls, topic, *, channel: str=None, **kwargs):
        """Instance signature
        """
        self = super().__new__(cls)
        self.topic = topic
        self.channel = channel
        return self

    def __aiter__(self):
        return self

    async def __anext__(self):
        return (await self.get())

    def __repr__(self):
        return '<{}: {}>'.format(
            self.__class__.__name__, ':'.join([self.topic, self.channel]))
