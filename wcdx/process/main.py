import logging
import asyncio
import argparse
from wcdx.config import settings
from wcdx.util import set_cancel_handler
from wcdx.process.handler import collect
from wcdx.store.connection import init_db

logger = logging.getLogger(__name__)


def run():
    """Main collect loop
    """
    async def go(loop):
        db = await init_db(loop)

        try:
            while True:
                await collect(db, loop)
                await asyncio.sleep(60*2)
        except asyncio.CancelledError:
            logger.info('stopping collect loop')
        except Exception:
            logger.exception('unknown error')
        finally:
            return

    loop = asyncio.get_event_loop()
    set_cancel_handler(loop)

    asyncio.ensure_future(go(loop), loop=loop)

    try:
        loop.run_forever()
    finally:
        loop.close()


parser = argparse.ArgumentParser(
    description='Collect data')
parser.add_argument(
    '-c', '--config',
    action="store",
    help='Configuration file',
    type=str, nargs='?')

args = parser.parse_args()
# set up config
settings.setup(args.config)

if __name__ == '__main__':
    run()
