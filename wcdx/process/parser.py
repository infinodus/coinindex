import logging
from wcdx.config import settings
from datetime import datetime, timezone

logger = logging.getLogger(__name__)


async def prepare(data: dict, history: dict=None):
    """Parse API call response data

    Parse API call response data and yield
    consecutive point and volume objects.

    The first listed currency of a currency pair is called the (base) currency,
    the second currency is called the quote currency.
    """
    history = history or {}
    accepted = set(settings.getlist('currency', 'accepted'))

    for item in data.get('Markets'):
        # currency is always matched with BTC, hence _
        currency, _ = [
            e.strip().upper() for e in item.get('Label').split('/')
        ]

        # accepted check
        if currency not in accepted:
            continue

        timestamp = datetime.fromtimestamp(
            item.get('Timestamp'), tz=timezone.utc)

        # timestamp check
        if currency in history and timestamp <= history[currency]:
            continue

        for key, value in item.items():
            if key.startswith('Price'):
                quote = key.split('_')[1].upper()
                if quote not in accepted:
                    continue

                yield {
                    'type': 'point',
                    'data': {
                        'currency': currency,
                        'quote': quote,
                        'value': value,
                        'timestamp': timestamp
                    }
                }

            if key.startswith('Volume'):
                yield {
                    'type': 'volume',
                    'data': {
                        'currency': currency,
                        'value': value,
                        'timestamp': timestamp
                    }
                }
