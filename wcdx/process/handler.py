import logging
from wcdx.util import as_future
from wcdx.process.parser import prepare
from wcdx.collect.client import get_data
from wcdx.store.models import Point, Volume
from clickhouse_sqlalchemy import make_session

logger = logging.getLogger(__name__)


@as_future
def get_currency_timestamp(db, days: int=7, loop=None):
    """Return last update timestamp for a currency volume
    """
    from sqlalchemy import func

    result = make_session(db) \
        .query(Volume.currency, func.max(Volume.timestamp)) \
        .group_by(Volume.currency).all()

    return dict(result)


@as_future
def add_entries(db, prepared, loop=None):
    """Add entries
    """
    def chunk(prepared, size: int=500):
        batch = []
        for entry in prepared:
            if entry['type'] == 'volume':
                item = Volume(**entry['data'])
            if entry['type'] == 'point':
                item = Point(**entry['data'])

            if len(batch) == size:
                yield batch
                batch = []
            batch.append(item)
        if batch:
            yield batch

    session = make_session(db)

    for batch in chunk(prepared):
        session.add_all(batch)
        session.flush()
    session.commit()


async def collect(db, loop=None):
    """Collect and process entries
    """
    try:
        data = await get_data()
    except Exception:
        logger.warning('unable to collect')
        return

    # last pair updates
    history = await get_currency_timestamp(db, loop=loop)

    # prepare data
    prepared = [item async for item in prepare(data, history)]

    if prepared:
        # add to clickhouse
        await add_entries(db, prepared, loop=loop)

        # items to queue

        logger.info('processed {} items'.format(len(prepared)))

    return len(prepared)
